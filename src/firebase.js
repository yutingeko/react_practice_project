import * as firebase from 'firebase'

const config = {
  apiKey: "AIzaSyDvokZMzs4O8QABtwc1TnG-mpIFqhDuaIk",
  authDomain: "nba-data-3ad8f.firebaseapp.com",
  databaseURL: "https://nba-data-3ad8f.firebaseio.com",
  projectId: "nba-data-3ad8f",
  storageBucket: "nba-data-3ad8f.appspot.com",
  messagingSenderId: "226746874598"
};

firebase.initializeApp(config);

const firebaseDB = firebase.database();
const firebaseArticles = firebaseDB.ref('articles')
const firebaseTeams = firebaseDB.ref('teams')
const firebaseVideos = firebaseDB.ref('videos')

const firebaseLooper = (snapshot) => {
  const data = [];
  snapshot.forEach(childSnapshot => {
    data.push({
      ...childSnapshot.val(),
      id: childSnapshot.key
    });
  })
  return data;
}

export {
  firebase,
  firebaseDB,
  firebaseArticles,
  firebaseTeams,
  firebaseVideos,
  firebaseLooper
}