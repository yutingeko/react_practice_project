import React, { Component } from 'react';
import { firebase, firebaseArticles, firebaseLooper } from '../../../firebase';

import SliderTemplates from './slider_templates';

class NewsSlider extends Component {

  state = {
    news: []
  }

  UNSAFE_componentWillMount() {
    firebaseArticles.limitToFirst(3).once('value')
    .then(snapshot => {
      const news = firebaseLooper(snapshot)

      //let requests = [promise1, promise2, promise3]
      let requests = news.map((item, i) => {
        return new Promise((resolve) => {
          firebase.storage().ref('images')
          .child(item.image).getDownloadURL()
          .then(url => {
            news[i].image = url;
            resolve();
          })
        })
      })

      Promise.all(requests).then(() => {
        this.setState({news});
      })
    })
    .catch(e => console.log(e))
  }

  render() {
    return (
      <SliderTemplates data={this.state.news} type={this.props.type} settings={this.props.settings}/>
    );
  }
}

export default NewsSlider;