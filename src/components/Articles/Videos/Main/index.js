import React from 'react';

import VideoList from '../../../widgets/VideoList/videoList'

const VideoMain = () => {
  return (
    <div>
      <VideoList
        type="card"
        title={false}
        loadmore={true}
        start={0}
        amount={8}
      />
    </div>
  );
};

export default VideoMain;