import React, { Component } from 'react';
import styles from './signin.css'
import { firebase } from '../../firebase'

import FormField from '../widgets/FormFields/formFields'

class SignIn extends Component {

  state = {
    registerError: '',
    loading: false,
    formData: {
      email: {
        element: 'input',
        value: '',
        config: {
          type: 'input',
          name: 'enail_input',
          placeholder: 'Enter your email'
        },
        validation: {
          required: true,
          email: true
        },
        valid: false,
        touched: false,
        validationMessage: ''
      },
      password: {
        element: 'input',
        value: '',
        config: {
          type: 'password',
          name: 'password_input',
          placeholder: 'Enter your password'
        },
        validation: {
          required: true,
          password: true
        },
        valid: false,
        touched: false,
        validationMessage: ''
      },
    }
  }

  updateForm = element => {
    const newFormData = { //copy formData
      ...this.state.formData
    }
    const newElement = {  //copy element
      ...newFormData[element.id]
    }
    newElement.value = element.event.target.value;

    if(element.blur) {
      let validData = this.validate(newElement)
      newElement.valid = validData[0];
      newElement.validationMessage = validData[1];
    }

    newElement.touched = element.blur;
    newFormData[element.id] = newElement;

    this.setState({
      formData: newFormData
    })
  }

  validate = (element) => {
    let error = [true, '']; //[[Boolean], ["ErrorMessage"]]

    if(element.validation.required) {
      const valid = element.value.trim() !== '';
      const message = `${!valid ? 'The field is required' : ''}`;
      error = !valid ? [valid, message] : error
    }

    if(element.validation.password) {
      const valid = element.value.length >= 5;
      const message = `${!valid ? 'Must be greater than 5' : ''}`;
      error = !valid ? [valid, message] : error
    }

    if(element.validation.email) {
      const valid = /\S+@\S+\.\S+/.test(element.value);
      const message = `${!valid ? 'Must be a valid email' : ''}`;
      error = !valid ? [valid, message] : error
    }

    return error;
  }

  submitButton = () => (
    this.state.loading ?
    'loading...'
    :
    <div>
      <button onClick={(event) => this.submitForm(event, false)}>Register now</button>
      <button onClick={(event) => this.submitForm(event, true)}>Log in</button>
    </div>
  )

  submitForm = (event, isMember) => {
    event.preventDefault();
    if(isMember !== null) {
      let dataToSubmit = {};
      let formIsValid = true;

      for(let key in this.state.formData) {
        dataToSubmit[key] = this.state.formData[key].value
        formIsValid = this.state.formData[key].valid && formIsValid
      }

      if(formIsValid) {
        this.setState({
          loading: true,
          registerError: ''
        })

        if(isMember) {
          firebase.auth()
          .signInWithEmailAndPassword(
            dataToSubmit.email,
            dataToSubmit.password
          ).then(() => {
            this.props.history.push('/')
          }).catch(error => {
            this.setState({
              loading: false,
              registerError: error.message
            })
          })
        } else {
          firebase.auth()
          .createUserWithEmailAndPassword(
            dataToSubmit.email,
            dataToSubmit.password
          ).then(() => {
            this.props.history.push('/')
          }).catch(error => {
            this.setState({
              loading: false,
              registerError: error.message
            })
          })
        }
      }
    }
  }

  render() {
    return (
      <div className={styles.logContainer}>
        <form onSubmit={(event) => this.submitForm(event, null)}>
          <h2>Register / Log in</h2>
          <FormField
            id={'email'}
            formdata={this.state.formData.email}
            change={element=> this.updateForm(element)}
          />
          <FormField
            id={'password'}
            formdata={this.state.formData.password}
            change={element=> this.updateForm(element)}
          />
          {this.submitButton()}
        </form>
      </div>
    );
  }
}

export default SignIn;