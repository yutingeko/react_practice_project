# 使用React, React Router 與 Firebase實作簡易網站(以NBA為主題)

## 1. 主要功能
* 註冊/登入 登出
* 上傳圖片並發佈球隊訊息

## 2. Demo
[Demo](https://nba-data-3ad8f.firebaseapp.com/)
